const express = require('express')
const router = express.Router()
const amqp = require('amqplib/callback_api')


router.get('/',(req,res) => {
    let result = []
    amqp.connect((error0,connection) => {
        if (error0) {
            return res.send(error0)
        }
        console.log('Connected...')
    
        connection.createChannel((error1,channel) => {
            if (error1) return console.log(error)
    
            let queue = 'hello'
    
            channel.assertQueue(queue, {
                durable:  false
            })
            console.log(`[x] Wating for messge in ${queue}.... \n Press CTRL + C to Exit`)
    
            channel.consume(queue, (msg) => {
                res.send(`${msg.content.toString()}`)
            },{
                noAck: true
            })
        })
    })    
    
})

module.exports = router