const express = require('express')
const router = express.Router()
const amqp = require('amqplib/callback_api')


router.get('/:msg',(req,res) => {
    amqp.connect('amqp://localhost', (error0,connection) => {
        if (error0) {
            return res.send(error0)
        }
        console.log('Connected...')
        connection.createChannel((error1,channel) => {
            if (error1) {
                return res.send(error)
            }
            console.log('Channel Created...')
            let queue = 'hello'
            let message = req.params.msg
    
            channel.assertQueue(queue, {
                durable: false
            })
    
            channel.sendToQueue(queue, Buffer.from(message))
            res.send(`[x] Sent ${message}`)
        })
    })    
})

module.exports = router
