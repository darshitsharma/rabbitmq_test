const app = require('express')()
const PORT = process.env.PORT || 3000
const send = require('./Services/send')
const recive = require('./Services/recive')

app.use('/send',send)
app.use('/recive',recive)
app.listen(PORT, (error) => {
    if (error) return console.log(error)
    console.log(`Listening on PORT ${PORT}`)
})